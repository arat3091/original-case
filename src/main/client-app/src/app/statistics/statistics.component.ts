import {Component, OnInit} from '@angular/core';
import {StatisticsService} from '../services/statistics.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  stats$: Observable<any>;

  constructor(private statisticsService: StatisticsService) {
  }

  ngOnInit() {
    this.stats$ = this.statisticsService.getStatistics();
  }

}
