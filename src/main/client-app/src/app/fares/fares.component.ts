import {Component, OnInit} from '@angular/core';
import {FormControl, Validator, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/internal/operators';
import {AirportsService} from '../services/airports.service';
import {FareDetails, Location} from '../services/model';
import {FaresService} from '../services/fares.service';

@Component({
  selector: 'app-fares',
  templateUrl: './fares.component.html',
  styleUrls: ['./fares.component.css']
})
export class FaresComponent implements OnInit {

  originCtrl: FormControl;
  destinationCtrl: FormControl;
  filteredOriginStates: Observable<Location[]>;
  filteredDestinationStates: Observable<Location[]>;
  fareDetails: FareDetails | undefined = undefined;


  constructor(private airportService: AirportsService, private faresService: FaresService) {
    this.originCtrl = new FormControl('', Validators.required);
    this.destinationCtrl = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.filteredOriginStates =
      this.originCtrl
        .valueChanges
        .pipe(
          startWith(''),
          debounceTime(400),
          distinctUntilChanged(),
          switchMap(origin => this.airportService.getAirport(origin)
            .pipe(map(value => value.locations)))
        );

    this.filteredDestinationStates =
      this.destinationCtrl
        .valueChanges
        .pipe(
          startWith(''),
          debounceTime(400),
          distinctUntilChanged(),
          switchMap(destination => this.airportService.getAirport(destination)
            .pipe(map(value => value.locations)))
        );
  }

  public getFares() {
    const origin = this.originCtrl.value;
    const destination = this.destinationCtrl.value;
    if (origin !== '' && destination !== '') {
      this.faresService.getFareDetails(origin, destination).subscribe(
        value => this.fareDetails = value,
        () => this.fareDetails = undefined
      );
    }
  }

}
