import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FaresComponent} from './fares/fares.component';
import {AirportsComponent} from './airports/airports.component';
import {StatisticsComponent} from './statistics/statistics.component';

const routes: Routes = [
  {path: 'fares', component: FaresComponent},
  {path: 'airports', component: AirportsComponent},
  {path: 'statistics', component: StatisticsComponent},
  {path: '', redirectTo: '/fares', pathMatch: 'full'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
