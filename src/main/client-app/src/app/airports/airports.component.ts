import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {startWith, switchMap} from 'rxjs/internal/operators';
import {AirportsService} from '../services/airports.service';
import {FormControl} from '@angular/forms';
import {merge, Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-airports',
  templateUrl: './airports.component.html',
  styleUrls: ['./airports.component.css']
})
export class AirportsComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns = ['name', 'description', 'code'];
  totalElements;
  filterFormCtrl: FormControl;
  pageSub: Subscription;
  filterSub: Subscription;

  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.pageSub = this.paginator.page.pipe(
      startWith({}),
      switchMap(() => {
        return this.airportService.getAirport(this.filterFormCtrl.value || '', this.paginator.pageIndex + 1, this.paginator.pageSize);
      }),
    ).subscribe(data => {
      this.dataSource.data = data.locations;
      this.totalElements = data.page.totalElements;
    });

    this.filterSub = this.filterFormCtrl.valueChanges.pipe(
      switchMap(() => {
        this.paginator.pageIndex = 0;
        return this.airportService.getAirport(this.filterFormCtrl.value || '', this.paginator.pageIndex + 1, this.paginator.pageSize);
      }),
    ).subscribe(data => {
      this.dataSource.data = data.locations;
      this.totalElements = data.page.totalElements;
    });

  }


  constructor(private airportService: AirportsService) {
    this.filterFormCtrl = new FormControl();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.filterSub) {
      this.filterSub.unsubscribe();
    }

    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }
  }


}
