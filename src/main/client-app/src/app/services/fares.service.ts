import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {Http} from '@angular/http';
import {FareDetails} from './model';

@Injectable({
  providedIn: 'root'
})
export class FaresService {

  private url = 'http://localhost:9000/travel/fare';

  constructor(private http: Http) {
  }

  getFareDetails(origin: string, destination: string): Observable<FareDetails> {
    const formattedUrl = `${this.url}/${origin}/${destination}`;
    return this.http.get(formattedUrl).pipe(
      map(res => res.json())
    );
  }
}
