export interface Location {
  code: string;
  description: string;
  name: string;
}

export interface LocationWrapper {
  locations: Array<Location>;
  page: Page;
}

export interface Page {
  size: number;
  totalElements: number;
  totalPages: number;
}

export interface FareDetails {
  fare: Fare;
  originDetails: Location;
  destinationDetails: Location;
}

export interface Fare {
  amount: number;
  currency: number;
}
