import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {Location, LocationWrapper} from './model';
import {Http} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AirportsService {

  private url = 'http://localhost:9000/travel/airports';

  constructor(private http: Http) {
  }

  getAirport(term: string, page: number = 1, size: number = 25): Observable<LocationWrapper> {
    const formattedUrl = `${this.url}?term=${term}&page=${page}&size=${size}`;
    return this.http.get(formattedUrl).pipe(
      map(res => res.json())
    );
  }

}
