import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  private url = 'http://localhost:9000/travel/statistics';

  constructor(private http: Http) {
  }

  getStatistics(): Observable<any> {
    return this.http.get(this.url).pipe(
      map(res => res.json())
    );
  }
}
