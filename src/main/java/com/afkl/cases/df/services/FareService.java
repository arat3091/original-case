package com.afkl.cases.df.services;

import com.afkl.cases.df.controllers.AirportController;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.FareDetails;
import com.afkl.cases.df.model.Location;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

@Service
public class FareService {

	private static final Logger logger = LoggerFactory.getLogger(FareService.class);

	@Autowired
	private OAuth2RestTemplate oAuth2RestTemplate;

	@Value("${service.api.fareUrl}")
	private String fareUrl;

	@Async("serviceThreadExecutor")
	public CompletableFuture<Fare> getFare(String originCode, String destinationCode) throws Exception {
		logger.info("Running the task for getting the fare");
		String formattedUrl = String.format(fareUrl, originCode, destinationCode);
		return CompletableFuture.completedFuture(oAuth2RestTemplate
			.getForEntity(formattedUrl, Fare.class)
			.getBody());
	}
}
