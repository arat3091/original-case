package com.afkl.cases.df.services;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.stereotype.Service;

@Service
public class AppStatisticsService {

	@Autowired
	public MetricsEndpoint metricsEndpoint;

	public Map<String, Long> getApplicationStatistics() {
		Map<String, Long> stats = new HashMap<>();
		Map<String, Object> metricsStats = metricsEndpoint.invoke();

		Long totalOkRequest =
			getTotalFromMetrics(metricsStats, "counter.status.2");
		Long total4xxRequest =
			getTotalFromMetrics(metricsStats, "counter.status.4");
		Long total5xxRequest =
			getTotalFromMetrics(metricsStats, "counter.status.5");
		Long totalRequest =
			getTotalFromMetrics(metricsStats, "counter.status");

		stats.put("TotalOkrequest", totalOkRequest);
		stats.put("Total4xxRequest", total4xxRequest);
		stats.put("Total5xxRequest", total5xxRequest);
		stats.put("TotalRequest", totalRequest);

		if (metricsStats.get("gauge.request.minTime") != null && totalRequest != 0) {
			stats.put("minTime", getValue(metricsStats, "gauge.request.minTime"));
		}

		if (metricsStats.get("gauge.request.maxTime") != null && totalRequest != 0) {
			stats.put("maxTime", getValue(metricsStats, "gauge.request.maxTime"));
		}

		if (metricsStats.get("gauge.request.totalTime") != null && totalRequest != 0) {
			stats.put("totalTime", getValue(metricsStats, "gauge.request.totalTime"));

			Long averageTime = getValue(metricsStats, "gauge.request.totalTime") / totalRequest;
			stats.put("averageTime", averageTime);

		}
		return stats;
	}

	private Long getTotalFromMetrics(Map<String, Object> metricsStats, String key) {
		return metricsStats
			.entrySet()
			.stream()
			.filter(s -> !s.getKey().contains(".metrics"))
			.filter(s -> s.getKey().startsWith(key))
			.mapToLong(s -> Long.parseLong(s.getValue().toString()))
			.sum();
	}

	private Long getValue(Map<String, Object> metricsStats, String key) {
		try {
			return NumberFormat
				.getInstance()
				.parse(metricsStats.get(key).toString()).longValue();

		} catch (ParseException e) {
			e.printStackTrace();
			return 0L;
		}
	}

}
