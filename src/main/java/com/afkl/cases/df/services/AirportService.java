package com.afkl.cases.df.services;

import com.afkl.cases.df.model.Location;
import com.afkl.cases.df.model.LocationWrapper;
import com.afkl.cases.df.model.LocationWrapperSearchResult;
import com.afkl.cases.df.model.Page;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

@Service
public class AirportService {

	private static final Logger logger = LoggerFactory.getLogger(AirportService.class);

	@Autowired
	private OAuth2RestTemplate oAuth2RestTemplate;

	@Value("${service.api.airportsUrl}")
	private String airportsUrl;

	@Value("${service.api.airportUrl}")
	private String airportUrl;

	public LocationWrapperSearchResult getAirports(String term, String pageNumber, String size) {
		String formattedUrl = String.format(airportsUrl, term, pageNumber, size);
		try {
			LocationWrapper locationWrapper = oAuth2RestTemplate
				.getForEntity(formattedUrl, LocationWrapper.class)
				.getBody();

			return LocationWrapperSearchResult.builder()
				.locations(locationWrapper.getEmbedded().getLocations())
				.page(locationWrapper.getPage()).build();

		} catch (RestClientException ex) {
			return LocationWrapperSearchResult.builder()
				.locations(new ArrayList<Location>())
				.page(Page.builder().totalElements(0L).build())
				.build();
		}
	}

	@Async("serviceThreadExecutor")
	public CompletableFuture<Location> getAirport(String code) {
		logger.info("running task for getting the Airport detail for code" + code);
		String formattedUrl = String.format(airportUrl, code);
		return CompletableFuture.completedFuture(oAuth2RestTemplate
			.getForEntity(formattedUrl, Location.class)
			.getBody());
	}
}
