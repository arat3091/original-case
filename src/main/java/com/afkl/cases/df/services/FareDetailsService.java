package com.afkl.cases.df.services;

import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.FareDetails;
import com.afkl.cases.df.model.Location;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FareDetailsService {

	@Autowired
	public FareService fareService;

	@Autowired
	public AirportService airportService;

	public FareDetails getFareDetails(String originCode, String destinationCode) throws Exception {

		// below all three call are async calls and will be done in parallel
		CompletableFuture<Fare> fare = fareService.getFare(originCode, destinationCode);
		CompletableFuture<Location> originLocation = airportService.getAirport(originCode);
		CompletableFuture<Location> destinationLocation = airportService.getAirport(destinationCode);

		// main thread will be blocked  till all three call completes
		CompletableFuture.allOf(
			fare,
			originLocation,
			destinationLocation
		).join();

		return FareDetails.builder()
			.fare(fare.get())
			.originDetails(originLocation.get())
			.destinationDetails(destinationLocation.get())
			.build();
	}
}
