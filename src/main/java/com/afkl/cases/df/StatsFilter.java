package com.afkl.cases.df;

import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Component;

@Component
public class StatsFilter implements Filter {
	private static AtomicLong minTime;
	private static AtomicLong maxTime;
	private static AtomicLong totalTime;

	@Autowired
	private GaugeService gaugeService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws java.io.IOException, ServletException {

		long startTime = System.currentTimeMillis();
		chain.doFilter(request, response);
		long endTime = System.currentTimeMillis();

		long timeTaken = endTime - startTime;

		if (minTime == null) {
			minTime = new AtomicLong(timeTaken);
		} else {
			minTime.set(Math.min(minTime.get(), timeTaken));
		}

		if (maxTime == null) {
			maxTime = new AtomicLong(timeTaken);
		} else {
			maxTime.set(Math.max(maxTime.get(), timeTaken));
		}

		if (totalTime == null) {
			totalTime = new AtomicLong(timeTaken);
		} else {
			totalTime.getAndAdd(timeTaken);
		}

		gaugeService.submit("gauge.request.minTime", minTime.get());
		gaugeService.submit("gauge.request.maxTime", maxTime.get());
		gaugeService.submit("gauge.request.totalTime", totalTime.get());
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {
	}
}
