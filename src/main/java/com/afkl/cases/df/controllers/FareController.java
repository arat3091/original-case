package com.afkl.cases.df.controllers;

import com.afkl.cases.df.model.FareDetails;
import com.afkl.cases.df.services.FareDetailsService;
import com.afkl.cases.df.services.FareService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(value = "fare/{originCode}/{destinationCode}")
public class FareController {

	@Autowired
	public FareDetailsService fareDetailsService;

	@RequestMapping(method = RequestMethod.GET,
		produces = "application/json")
	public FareDetails getFareDetails(
		@PathVariable String originCode,
		@PathVariable String destinationCode
	) throws Exception {
		return fareDetailsService.getFareDetails(originCode, destinationCode);
	}
}
