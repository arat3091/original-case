package com.afkl.cases.df.controllers;

import com.afkl.cases.df.model.Location;
import com.afkl.cases.df.model.LocationWrapperSearchResult;
import com.afkl.cases.df.services.AirportService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/airports")
public class AirportController {

	@Autowired
	private AirportService airportService;

	private static final Logger logger = LoggerFactory.getLogger(AirportController.class);

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public LocationWrapperSearchResult getAirports(
		@RequestParam(value = "term", required = false, defaultValue = "") String term,
		@RequestParam(value = "page", required = false, defaultValue = "1") String page,
		@RequestParam(value = "size", required = false, defaultValue = "25") String size) {
		logger.info("Airport Controller");
		return airportService.getAirports(term, page, size);
	}
}
