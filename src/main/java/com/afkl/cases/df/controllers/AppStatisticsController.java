package com.afkl.cases.df.controllers;

import com.afkl.cases.df.services.AppStatisticsService;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/statistics")
public class AppStatisticsController {

	@Autowired
	public AppStatisticsService appStatisticsService;

	@GetMapping(produces = "application/json")
	public Map<String, Long> getStatistics() {
		return appStatisticsService.getApplicationStatistics();
	}
}
