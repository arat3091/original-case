package com.afkl.cases.df.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FareDetails {
	Fare fare;
	Location originDetails;
	Location destinationDetails;
}
