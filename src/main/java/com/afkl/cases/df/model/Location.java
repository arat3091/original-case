package com.afkl.cases.df.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;

import lombok.Builder;
import lombok.Data;

@JsonInclude(NON_NULL)
@Data
@Builder
public class Location {
	private String code;
	private String name;
	private String description;
	private Location parent;
	@JsonIgnore
	private Set<Location> children;
}
