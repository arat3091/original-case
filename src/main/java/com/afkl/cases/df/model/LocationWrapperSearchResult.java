package com.afkl.cases.df.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@JsonInclude(NON_NULL)
@Data
@Builder
public class LocationWrapperSearchResult {
	private List<Location> locations;
	private Page page;
}
