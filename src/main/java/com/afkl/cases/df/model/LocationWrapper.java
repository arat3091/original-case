package com.afkl.cases.df.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
public class LocationWrapper {

	public LocationWrapper() {
	}

	@JsonProperty("_embedded")
	private Embedded embedded;

	private Page page;

	@Data
	public class Embedded {
		public Embedded() {
		}

		private List<Location> locations;
	}
}
