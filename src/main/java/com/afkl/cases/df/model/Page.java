package com.afkl.cases.df.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;

@JsonInclude(NON_NULL)
@Data
@Builder
public class Page {
	private long size;
	private long totalElements;
	private long totalPages;
	private long number;
}
