Solution By Ankur Ratra

How To Run 

Go to client-app directory
do npm install
do npm start

Go to main project directory

do ./gradlew buildRum
start the mock

http://localhost:4200





1) Tech Stack
FrontEnd: Angular 2 , Rxjs , Angular material.
Backend: Spring Boot application with Java 8


2) FrontEnd:-  It has menu with three option. 
               a) Fare search b) Airport List c) Statistics
               
               a) Fare Search :- Its a reactive page with the ability to select the origin and destination.
               Origin or destination is an autocomplete input which shows first 25 locations by default.
               It shows only 25 as locations could  be very large and hence cam slow down the page.
               As we type , request goes to the backend and get the closest 25 locations matches.
               
               It has a search button and when clicked it shows the fare , origin and destination details.
               
               b) Airport List
               
               It shows a paginated (server side pagination) table showing the airport list. By default it shows 10 locations
               and we can use the paginator to get the next page which makes the request again to get next 10.
               Very efficient and performance. It can show the airport list which can run in millions also.
               
               It has global filter also which filters the records in the table. Power of RXJS is used.
               
               c) Statistics: Its a angular component showing the Application statistics regarding number or requests.
               
               
               d) Modular build is also set up with angular cli which used npm and webpack underneath.
     
 3) Backend:- Spring boot application containing 3 controllers for airport , fare and statistics.
              Below are the details.
              
              a) For the fare details three Async tasks are used to get the fare , origin and destination details
                 which run in different threads. We can see the thread name start with async-task-{thread id}.
                  See the ThreadConfig class where thread executor is set up.
                  
              b) Project Lombok is used to get the getters, setters and builders.
              
              c) Unique Id for each request is logged with logger. Spring cloud Sleuth is used for this purpose.
              
              d) Spring actuater is used to gather the statistics about the application rest calls.
              
              e) One Filter is also written to track the min , max  and total time taken for the rest calls.
              
              f) Airport controller serves the paginated and filtered airports or locations.
              
              g) No hard coded values for the end points are used . Everything is configured in application.yml
                